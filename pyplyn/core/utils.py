from pyplyn.core.errors import UnknownNext


def simple_next(next_config):
    return lambda command, item: next_config

def parse_config_func(next_config):
    if len(next_config) > 1:
        raise UnknownNext('Wrong function config:\n{}\nMultiple keys are not allowed!!!'.format(json.dumps(next_config, indent = 4)))

    func = list(next_config.keys())[0]
    args = next_config[func]
    if func == 'P:if':
        return lambda command, item: make_next(args[1])(command, item) if make_next(args[0])(command, item) else make_next(args[2])(command, item)
    elif func == 'P:or':
        return lambda command, item: make_next(args[0])(command, item) or make_next(args[1])(command, item)
    elif func == 'P:and':
        return lambda command, item: make_next(args[0])(command, item) and make_next(args[1])(command, item)
    elif func == 'P:attr':
        return lambda command, item: make_next(item[args])(command, item) if args in item else None # if hasattr(item, args) else None
    elif func == 'P:eq':
        return lambda command, item: make_next(args[0])(command, item) == make_next(args[1])(command, item)
    elif func == 'P:not':
        return lambda command, item: not make_next(args)(command, item)
    else:
        raise UnknownNext('Unrecognized key: {} in {}'.format(func, json.dumps(next_config, indent=4)))

def make_next(next_config):
    if type(next_config) is str:
        if next_config == 'free':
            return lambda command, item: command
        return simple_next(next_config)
    elif type(next_config) is dict:
        return parse_config_func(next_config)
    elif type(next_config) is bool:
        return lambda command, item: next_config
    else:
        raise UnknownNext('The next format of {} is not recognized!'.format(next_config))

def from_queue_to_queue(source_queue_name, target_queue_name, delete=True, visibility_timeout=60 * 30, limit=10000):
    import threading
    import boto3
    sqs = boto3.resource('sqs')
    source_queue = sqs.get_queue_by_name(QueueName = source_queue_name)
    target_queue = sqs.get_queue_by_name(QueueName = target_queue_name)

    def process_messages(i, lock):
        msgs = source_queue.receive_messages(MaxNumberOfMessages = 10, VisibilityTimeout = visibility_timeout)
        for msg in msgs:
            try:
                x = target_queue.send_message(MessageBody = msg.body)
            except:
                continue
            if delete:
                msg.delete()
            with lock:
                i[0] += 1

    i = [0]
    while True:
        try:
            threads = []
            lock = threading.Lock()
            for ti in range(16):
                t = threading.Thread(target = process_messages, args = (i, lock))
                t.start()
                threads.append(t)
            for t in threads:
                t.join()
            print('{} messages migrated ({} delete)'.format(i[0], 'with' if delete else 'without'))
            if i[0] >= limit:
               break
        except KeyboardInterrupt:
            break
    print('Exiting...')
