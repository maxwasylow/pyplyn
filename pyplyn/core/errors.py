class EditingNotDefinedKeys(BaseException): pass
class MissingRequiredError(BaseException): pass
class UnknownNext(BaseException): pass
