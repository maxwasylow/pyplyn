import threading
import traceback
import sys
from helpers import findpath, update
from helpers.loggers import getlogger

import simplejson as json

from pyplyn.core.errors import MissingRequiredError, EditingNotDefinedKeys
from pyplyn.core.utils import make_next

class Valve(object):

    lock = threading.Lock()

    def __init__(self, config):
        self.config = config

        self.env = self.config['env']
        self.loglevel = self.config['loglevel']

        self.input = self.config.get('input', [])
        self.required = self.config.get('required', [])
        self.add = self.config.get('add', [])
        self.edit = self.config.get('edit', [])
        self.drop = self.config.get('drop', {'after': [], 'before': []})
        self.lense = self.config.get('lense', None)
        self.lock_required = self.config.get('lock', False)
        if 'after' not in self.drop:
            self.drop['after'] = []
        if 'before' not in self.drop:
            self.drop['before'] = []
        self.process_mode = self.config.get('process', None)
        self.next = make_next(self.config.get('next', None))
        self.strict = self.config.get('strict', False)
        self.logger = getlogger(self.__class__.__name__, 'pyplyn-{}'.format(self.env), self.loglevel)

    def run(self, item):
        return item

    def check_input(self, item):
        missing_req = []
        for in_ in self.required:
            if not findpath(item, in_):
                missing_req.append(in_)
        if len(missing_req) > 0:
            raise MissingRequiredError('Missing required attribute: {} for item...'.format(missing_req))
        del missing_req
        missing_in = []
        for in_ in self.input:
            if in_ not in item:
                missing_in.append(in_)
        if len(missing_in) > 0:
            self.logger.debug('Missing input attributes: {} for item...'.format(missing_in))
        del missing_in
        return True

    def check_output(self, item, add, edit):
        not_valid_added = set(add.keys()).intersection(set(item.keys()))
        if len(not_valid_added) > 0:
            raise EditingNotDefinedKeys('[{}] keys are being added, but were not defined!'.format(not_valid_added))
        if self.strict:
            missing_added = set(self.add).difference(set(add.keys()))
            if len(missing_added) > 0:
                self.logger.warning('[{}] keys were defined to be added, but are not!'.format(missing_added))
            missing_edited = set(self.edit).difference(set(edit.keys()))
            if len(missing_edited) > 0:
                self.logger.warning('[{}] keys were defined to be edited, but are not!'.format(missing_edited))
            missing_dropped = set(self.drop['after']).difference(set(item.keys()))
            if len(missing_dropped) > 0:
                self.logger.warning('[{}] keys were meant to be dropped, but they don\'t exist in item!'.format(missing_dropped))
        return True

    def process(self, item, args):
        try:
            if self.process_mode == 'free':
                return self.run( item )
            drop = self.drop[ 'before' ]
            if drop:
                if drop[ 0 ] == '*':
                    for key in list(item.keys()):
                        if key not in drop[1:]:
                            del item[key]
                else:
                    for key in drop:
                        if key in item:
                            del item[key]
            self.check_input(item)
            if self.lock_required:
                with self.__class__.lock:
                    msg, add, edit, command, args = self.run(item, args)
            else:
                msg, add, edit, command, args = self.run(item, args)
            self.check_output(item, add, edit)
            item = update(item, dict(add, **edit), lense=self.lense)
            drop = self.drop['after']
            if drop:
                if drop[0] == '*':
                    for key in list(item.keys()):
                        if key not in drop[1:]:
                            del item[key]
                else:
                    for key in drop:
                        if key in item:
                            del item[key]
            return msg, self.next(command, item), args
        except BaseException as e:
            exc_desc = {
                'valve': self,
                'exception': e,
                'exception_info': sys.exc_info(),
                'item': item
            }
            return '', 'ErrorValve', exc_desc


class EndValve(BaseException):
    pass


class ErrorValve(Valve):
    config = {
        'next': 'EndValve',
        'loglevel': 'INFO'
    }

    def run(self, item, exc_desc):
        exc_format = {
            'valve_name': exc_desc['valve'].__class__.__name__,
            'exception_name': exc_desc['exception'].__class__.__name__,
            'exception': exc_desc['exception'],
            'traceback': '\n'.join(traceback.format_tb(exc_desc['exception_info'][2]))
        }
        error_info = '[{valve_name}] {exception_name}: {exception}\n{traceback}'.format(**exc_format)
        if 'delete_from_queue' in item:
            del item['delete_from_queue']
        self.logger.error(error_info)
        return 'Error Executed!', {}, {}, EndValve, (item, exc_desc, exc_format, error_info)
