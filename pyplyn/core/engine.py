import threading
import os
import time
import simplejson as json
import inspect
import logging
from decimal import Decimal

from queue import Queue, Empty, Full

from pyplyn.config import PyplynConfig, PyplynScripts, PyplynValves
from pyplyn.core.valves import Valve, EndValve

from helpers.loggers import getlogger
from helpers.funchelpers import meantime_cache
from helpers import hash_dir_contents, sha1OfFile, md5, ts, format_seconds

class PyplynEngine(object):

    def __init__(self, config):
        # Signals => used for communicating with processes
        self._stop = False
        self._fetchers_stopped = False
        self._pause = False

        # Hashes => used for detecting changes to config
        self.config_hash = None
        self.script_hash = None
        self.script_file_contents = None # Stores information about which functions are stored in which file
        self.valve_hash = None

        self._last_fetched_reprocess = ts()
        self._no_fetched_reprocess = 0
        self.reprocess_alias = 'Items'
        self._reprocess_empty_times = 0
        self._last_fetched_process = ts()
        self._no_fetched_process = 0
        self.process_alias = 'Items'
        self._process_empty_times = 0

        self.config = config
        self.config_hash = sha1OfFile(self.config.config_filepath) if os.path.exists(self.config.config_filepath) else None
        self.logger = getlogger('PyplynEngine-{}'.format(self.config.env), 'pyplyn-engine-{}'.format(self.config.env), self.config.loglevel, cloudwatch_level = self.config.loglevel)

        # Queues' initialization
        #     * Job Queue => Gets Items from the external source using `fetch` method
        #     * Process Queue => Stores Processed Items together with instructions what to do with them next
        #     * Reprocess Job Queue => Gets Items from the external source but with lower priority, 
        #                              for instance items that need to be reprocessed without clogging main job queue
        self.job_queue = Queue(maxsize = self.config.queue_size)
        # self.process_queue = Queue() # No max-size <- this queue gets processed first anyways
        self.reprocess_job_queue = Queue(maxsize = self.config.reprocess_queue_size)

        # Thread groups definitions
        #     * Subprocesses => Main workers carrying out jobs (processing items)
        #     * Main Fetchers => Threads that constantly fetch data from external source using `fetch` method
        #     * Reprocess Fetchers => Threads that constantly fetch data from external source with lower priority (`reprocess_fetch`)
        #     * Updater => Looks for updates made to any config files and carries out updates on the go
        self._subprocesses = []
        self._mainfetchers = []
        self._reprocessfetchers = []
        # self._updater = None
        self.__pause_confirmed = {}

        self.scripts = PyplynScripts(self.config)
        self.valves = PyplynValves(self.config)

        # Initializing _updater
        # t = threading.Thread(name='Updater', target=self._pipeline_updater)
        # t.daemon = True
        # self._updater = t
        # t.start()

        self.scripts.postconfig(self.config)
        self.itemdict = {} # Used in DEBUG mode to store information about processed items

        self.logger.info('PYPLYN STARTED!!!')


    def get_config(self):
        self.preconfig(self.config)
        new_config = PyplynConfig.from_file(self.config.config_filepath)
        if new_config.env != self.config.env:
            self.logger.error('Can\'t change Pyplyn ENV without restarting Pyplyn!!!')
            new_config.env = config.env
        if new_config.name != self.config.name:
            self.logger.error('Can\'t change Pyplyn NAME without restarting Pyplyn!!!')
        self.config = new_config
        self.logger.setLevel(logging.__dict__[self.config.loglevel])
        for handler in self.logger.handlers:
            handler.setLevel(logging.__dict__[self.config.loglevel])
        self.scripts.postconfig(self.config)
        self.config_hash = sha1OfFile(self.config.configfile)


    def start( self ):
        self.scripts.prestartup( self.config )

        # Starting stats thread if stats is enabled
        if self.config.stats:
            t = threading.Thread(name = 'Stats', target = self._thread_wrapper(self._pipeline_stats))
            t.daemon = True
            self._stats = t
            self.__pause_confirmed[t.ident] = False
            t.start()

        for i in range(self.config.processes):
            t = threading.Thread(name='Worker{}'.format(i), target=self._thread_wrapper(self._pipeline_worker))
            t.daemon = True
            self._subprocesses.append(t)
            self.__pause_confirmed[t.ident] = False
            t.start()

        t = threading.Thread(name='MainFetcher{}'.format(0), target=self._thread_wrapper(self._pipeline_main_fetcher))
        t.daemon = True
        self._mainfetchers.append(t)
        self.__pause_confirmed[t.ident] = False
        t.start()

        t = threading.Thread(name='ReprocessFetcher{}'.format(0), target=self._thread_wrapper(self._pipeline_reprocess_fetcher))
        t.daemon = True
        self._reprocessfetchers.append(t)
        self.__pause_confirmed[t.ident] = False
        t.start()

        self.scripts.poststartup( self.config )

    def stop(self):
        self.scripts.prestop(self.config)
        self._stop = True

        for fetcher in self._mainfetchers:
            fetcher.join()
        for fetcher in self._reprocessfetchers:
            fetcher.join()
        # if self._updater:
        #     self._updater.join()
        if self.config.stats and self._stats:
            self._stats.join()
        self.job_queue.join()
        # self.process_queue.join() # waiting for remaining jobs to be finished
        self.reprocess_job_queue.join()
        self._main_fetchers = []
        self._reprocess_fetchers = []
        # self._updater = None
        self._subprocesses = []
        self._stop = False
        self.logger.info('Successfully Stopped PyplynEngine')
        self.scripts.poststop(self.config)

    def pause(self):
        self.scripts.prepause(self.config)
        self._pause = True
        self.block_until(True)
        self.logger.info('Paused PyplynEngine')

    def unpause(self):
        self._pause = False
        self.block_until(False)
        self.scripts.postpause(self.config)
        self.logger.info('Unpaused PyplynEngine')

    def restart(self):
        self.stop()
        self.start()

    def update(self):
        config_updated = self.config.update()
        if config_updated:
            self.scripts = PyplynScripts(self.config)
            self.valves = PyplynValves(self.config)
        else:
            self.scripts.update()
            self.valves.update()

    def process_output(self, message, valve, next_command, args, item):
        if valve.loglevel in ('DEBUG', 'INFO'):
            valve.logger.info('[{}] {}: {}'.format(valve.__class__.__name__, message, item['pyplynid']))
        return item, next_command, args

    def process_input(queue, item, valve, engine=None):
        pass

    def get_next_job(self):
        try:
            item, valve, args = self.job_queue.get(block=False)
            return self.job_queue, item, valve, args
        except Empty:
            try:
                item, valve, args = self.reprocess_job_queue.get(block = False)
                return self.reprocess_job_queue, item, valve, args
            except Empty:
                return None

    def _thread_wrapper(self, actual_thread_function):
        def thread():
            stop = False
            while not self._stop or not stop:
                self.handle_pause(thread_type = actual_thread_function.__name__)
                stop = actual_thread_function()
            else:
                self.logger.info('Received STOP signal... Shutting down {}!'.format(actual_thread_function.__name__))
        return thread

    def handle_pause(self, thread_type='unknown'):
        if self._pause:
            self.__pause_confirmed[threading.get_ident()] = True
            self.logger.info('Received PAUSE signal... Pausing {}!'.format(thread_type))
            while self._pause:
                time.sleep(1)
            else:
                self.__pause_confirmed[threading.get_ident()] = False
                self.logger.info( 'Received UNPAUSE signal... Resuming {}!'.format( thread_type ) )

    def _pipeline_worker(self):
        """
            Pipeline worker thread =>
                gets jobs from queues, makes sure they are carried out,
                and puts them back in the queue with next command statements
        """
        next_job = self.get_next_job()
        if next_job:
            _q, item, command, args = next_job
            try:
                while True:
                    valve = self.valves.from_command(command)
                    # self.process_input( _q, item, valve )
                    message, next_command, args = valve.process(item, args)
                    item, command, args = self.process_output(message, valve, next_command, args, item)
                    # self.process_queue.put( next_input )
            except EndValve as e:
                self.logger.debug('EndValve detected for: {}'.format(item['pyplynid']))
            except Exception as e:
                self.handle_exception(e, item)
            finally:
                _q.task_done()
        else:
            sleep_for = 2
            for _ in range(sleep_for):
                if self._stop and self._fetchers_stopped:
                    return True
                else:
                    time.sleep(1)

    def handle_exception(self, exception, item):
        if 'delete_from_queue' in item:
            del item['delete_from_queue']
        item['pyplyn_start'] = float(item[ 'pyplyn_start'])
        try:
            raise exception
        except Exception as e:
            self.logger.exception(json.dumps(item, indent=4))
            self.logger.critical(f'ITEM HAS FAILED AND THE ERROR WAS DETECTED ON THE PYPLYN ENGINE LEVEL!!!')

    def _pipeline_main_fetcher(self):
        """
            Runs prefetch -> fetch -> postfetch scripts
                and calls init_item putting all the returned items in job_queue
        """
        self.scripts.prefetch(self.config)
        fetched = 0
        for item in self.scripts.fetch(self.config):
            self.init_item(item, destination_queue=self.job_queue)
            fetched += 1
        if fetched:
            self._no_fetched_process += fetched
            self._process_empty_times = 0
        if ts() - self._last_fetched_process > 60:
            queue_size = self.job_queue.qsize()
            msg = f'Fetched {self._no_fetched_process} {self.process_alias} (last minute)! [Q size: {queue_size}]'
            self.logger.info(msg)
            self._no_fetched_process = 0
            self._last_fetched_process = ts()
        if not fetched:
            sleep_for = min(2**self._process_empty_times, 5)
            self._process_empty_times += 1
            for _ in range(sleep_for):
                if self._stop:
                    return True
                else:
                    time.sleep(1)
        return True

    def init_item( self, item, destination_queue ):
        """
            Prepares an `item` and inserts it in the `destination_queue`
            Args:
                item -- an item to be processed, or a tuple (item, command)
        """
        if type(item) is tuple:
            command = item[1]
            item = item[0]
        else:
            command = self.valves.first('first', item)

        item['pyplynid'] = md5( str( ts( precision = True ) ) )
        item['pyplyn_start'] = Decimal( str( ts( precision = True ) ) )

        complete = False
        while not complete:
            try:
                destination_queue.put( (item, command, ()), block = False )
                complete = True
            except Full:
                continue

    def _pipeline_reprocess_fetcher(self):
        """
            Prepares an item an inserts it in the reprocess_queue
        """
        self.scripts.reprocess_prefetch(self.config)
        fetched = 0
        for item in self.scripts.reprocess_fetch(self.config):
            self.init_item(item, destination_queue=self.reprocess_job_queue)
            fetched += 1
        if fetched:
            self._no_fetched_reprocess += fetched
            self._process_empty_times = 0
        if ts() - self._last_fetched_reprocess > 60:
            queue_size = self.reprocess_job_queue.qsize()
            msg = f'Fetched {self._no_fetched_reprocess} {self.reprocess_alias} (last minute)! [Q size: {queue_size}]'
            self.logger.info(msg)
            self._no_fetched_reprocess = 0
            self._last_fetched_reprocess = ts()
        if not fetched:
            sleep_for = min(2**self._reprocess_empty_times, 60)
            self._reprocess_empty_times += 1
            for _ in range(sleep_for):
                if self._stop:
                    return True
                else:
                    time.sleep(1)
        return True

    # def _pipeline_updater( self ):
    #     self.update()
    #     time.sleep( self.config.update_period )

    def _pipeline_stats(self):
        for i in range( 30 ):
            time.sleep( 1 )
            if self._stop:
                break
        if len(meantime_cache) > 0 and not self._stop and not self._pause:
            msg = [ '' ]
            meantime_log = []
            for k in meantime_cache:
                meantime_log.append( ( k, *meantime_cache[ k ] ) )
            max_time = max( [ x[ 1 ] for x in meantime_log ] )
            max_len = max( [ len( x[ 0 ] ) for x in meantime_log ] )
            meantime_log.sort( key = lambda x: x[ 1 ], reverse = True )
            for key, val, t in meantime_log:
                spaces = ' ' * ( max_len - len( key ) + 5 )
                total = format_seconds( ( v * t ) / self.config.processes, '%hh %mm %ss.%msms' )
                msg.append( f'{key}{spaces}{val:3.10f} ({t}) TOTAL: {total}' )
            for key, val, t in meantime_log:
                spaces = ' ' * ( max_len - len( key ) + 5 )
                no_stars = int( round( 50 * ( val / max_time ) ) )
                no_spaces = 50 - no_stars
                msg.append( f'{key}{spaces}|{"*"*no_stars}{" "*no_spaces}|' )
            msg = '\n'.join( msg )
            self.logger.warning( msg )
        return True
