import json
import copy

from helpers import get
from helpers.loggers import getlogger

from pyplyn.core.valves import Valve, ErrorValve, EndValve
from pyplyn.config.helper import DynamicDirConfig
from pyplyn.core.utils import make_next

class PyplynValves(DynamicDirConfig):
    def __init__(self, config):
        super(self.__class__, self).__init__('valves', config)
        self.env = config.env
        self.default_log_level = config.default_valve_loglevel

    def initialize(self, config):
        self.original_config = config
        self.base_valve_config = {
            'env': config.env,
            'loglevel': config.default_valve_loglevel
        }
        try:
            with open(config.valves_config_filepath) as valves_config:
                self.config = json.load(valves_config)
        except FileNotFoundError:
            raise Exception('You must create valve config file!')
        self.first = make_next(self.config['first'])
        self.last = make_next(self.config['last'])
        self.valves = self.config['Valves']

    def get_valve_config(self, valve_name):
        shared = {
            exposed_key: getattr(self.original_config, exposed_key, None) for exposed_key in (self.valves[valve_name]['exposed_config'] if 'exposed_config' in self.valves[valve_name] else [])
        }
        return {**self.base_valve_config, **shared, **self.valves[valve_name]}

    def preupdate(self):
        self.new_valve_names = []

    def postupdate(self):
        initialized = set()
        for valve_name in self.new_valve_names:
            if valve_name not in initialized:
                setattr(self, valve_name, getattr(self, valve_name)(self.get_valve_config(valve_name)))
                if isinstance(getattr(self, valve_name), ErrorValve):
                    self.error = getattr(self, valve_name)
                initialized.add(valve_name)
        if 'error' not in self.new_valve_names and not hasattr(self, 'error'):
            if 'ErrorValve' not in self.valves:
                self.valves['ErrorValve'] = ErrorValve.config
            self.error = ErrorValve(self.get_valve_config('ErrorValve'))

    def get_content_from_module(self, module):
        for attr_name in dir(module):
            attr = getattr(module, attr_name)
            if attr_name in self.valves and issubclass(attr, Valve):
                self.new_valve_names.append(attr_name)
                yield attr_name

    def from_command(self, command):
        if command is EndValve or command == 'EndValve':
            raise EndValve()
        elif command is ErrorValve or command == 'ErrorValve':
            return self.error
        if not hasattr(self, command):
            raise Exception('{} is not a valid command!!!'.format(command))
        return getattr(self, command)
