import json
import os
import types
import re
from collections import Counter

from ion.files import read
from ion.json import load, dump

from helpers import sha1OfFile, get
from helpers.funchelpers import type_check
from helpers.requesthelpers import get_remote_header, get_remote_config

class PyplynConfig:
    types = Counter({
        str: ['valves_config_filepath', 'config_filepath', 'loglevel',
              'default_valve_loglevel', 'scripts_dir', 'valves_dir', 'dir'],
        int: ['processes', 'queue_size', 'reprocess_queue_size'],
        float: ['update_period', 'update_frequency'],
        bool: ['strict', 'stats'],
        list: ['expose_to_valves']
    })
    defaults = dict(
        env=None,
        name=None,
        dir_=None,
        loglevel='INFO',
        default_valve_loglevel='INFO',
        processes=25,
        queue_size=50,
        reprocess_queue_size=50,
        scripts_dir='./pyplyn_scripts',
        valves_dir='./valves',
        valves_config_filepath=None,
        config_filepath=None,
        expose_to_valves=[],
        strict=False,
        stats=False,
        def_update_period=5,
        def_update_frequency=0.2
    )
    dump_types = (int, float, str)
    necessary = []

    def __init__(self, **kwargs):
        self._reserved_key_attrs = dir(self)
        kwargs = dict(self.__class__.defaults, **kwargs)
        if 'dir' in kwargs and kwargs['dir'] is not None:
            self.dir = kwargs['dir']
        else:
            try:
                import __main__
                self.dir = os.path.dirname(os.path.realpath(__main__.__file__))
            except AttributeError:
                self.dir = os.getcwd() # For interactive session usage
        if kwargs['env'] is None or kwargs['name'] is None:
            raise Exception('name: `{}` or env: `{}` is None!'.format(kwargs['name'], kwargs['env']))
        if not get(kwargs, 'config_filepath', None):
            kwargs['config_filepath'] = os.path.join(self.dir, 'config.json')
        if not get(kwargs, 'valves_config_path', None):
            kwargs['valves_config_filepath'] = os.path.join(self.dir, kwargs['valves_dir'], 'config.json')
        self.env = kwargs.pop('env')
        self.name = kwargs.pop('name')
        self.populate(**kwargs)

    def populate(self, **kwargs):
        type_check(kwargs, self.__class__.types, necessary=self.__class__.necessary)
        for attr_name in kwargs:
            if attr_name in self._reserved_key_attrs:
                raise Exception(f'Trying to assign attribute {attr_name} to {type(self).__name__} failed, because the argument is reserved!!!')
            else:
                setattr(self, attr_name, kwargs[attr_name])
        self.full_scripts_path = os.path.join(self.dir, self.scripts_dir) if self.scripts_dir[0] != '/' else self.scripts_dir
        self.full_scripts_path = re.sub(r'(?<!\.)\.\/', '', self.full_scripts_path)
        self.full_valves_path = os.path.join(self.dir, self.valves_dir) if self.valves_dir[0] != '/' else self.valves_dir
        self.full_valves_path = re.sub(r'(?<!\.)\.\/', '', self.full_valves_path)

    def update(self):
        pass

    def to_json(self):
        return dump({
            attr_name: getattr(self, attr_name)
            for attr_name in dir(self)
            if (attr_name not in self._reserved_key_attrs) and \
                isinstance(getattr(self, attr_name), self.__class__.dump_types)
        })

    @classmethod
    def from_file(cls, filepath):
        '''Creates a PyplynConfig instance from json file'''
        return cls(**load(read(filepath)))

    @classmethod
    def from_remote_config(cls, url, hash_header='X-Content-MD5', path=None, **kwargs):
        '''Creates PyplynConfig instance from url application/json response'''
        if path is None:
            path = [0, 'item', 'config']
        config_dict, _ = get_remote_config(url, hash_header, json_path=path)
        return cls(**config_dict, **kwargs)
