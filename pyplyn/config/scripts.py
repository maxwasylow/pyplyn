from pyplyn.config.helper import DynamicDirConfig

class PyplynScripts(DynamicDirConfig):

    allowed_scripts = ['prestartup', 'poststartup', 'fetch', 'preprocess', 'postprocess', 'prefetch', 'postfetch', 'preconfig', 'postconfig', 'prestop', 'poststop', 'prepause', 'postpause', 'reprocess_prefetch', 'reprocess_postfetch', 'reprocess_fetch']

    def __init__(self, config):
        super(self.__class__, self).__init__('scripts', config)


    def initialize(self, config):
        # self.content2module = {script_name: [] for script_name in self.__class__.allowed_scripts}
        for script_name in self.__class__.allowed_scripts:
            setattr(self, script_name, lambda config: None)


    def get_content_from_module(self, module):
        for script_name in self.__class__.allowed_scripts:
            if hasattr(module, script_name):
                yield script_name
