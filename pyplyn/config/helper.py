import os
import sys
import importlib
import re

from helpers import hash_dir_contents, file2modulename
from helpers.loggers import getlogger

class DynamicDirConfig(object):
    def __init__(self, _type, config):
        self._type = _type
        self.logger = getlogger('{}-{}'.format(self.__class__.__name__, config.env), '{}-{}'.format('-'.join(re.findall('[A-Z][^A-Z]*', self.__class__.__name__)), config.env), config.loglevel)
        self.dir = getattr(config, 'full_{}_path'.format(self._type))
        self.dir_name = getattr(config, '{}_dir'.format(self._type))

        self.module2contents = {}
        self.content2module = {}
        self.hash = {}

        self.initialize(config)
        self.update()

    def initialize(self, config):
        """Default initialize doesn't do anything"""
        pass

    def preupdate(self):
        pass

    def postupdate(self):
        pass

    def remove_module(self, modulename):
        if '/' in modulename or '.py' in modulename:
            modulename = file2modulename(modulename)
        if modulename in sys.modules:
            del sys.modules[modulename]

            found = None
            if modulename in self.module2contents:
                for content_elem in self.module2contents[modulename]:
                    for i, modname in self.content2module[content_elem]:
                        if modname == modulename:
                            found = i
                            break
                if found is not None:
                    del self.content2module[content_elem][found]
                del self.module2contents[modulename]

    def add_module(self, filename):
        modulename = file2modulename(filename) # Convert filename into python module name for import and lookup
        self.remove_module(modulename)
        module = importlib.import_module(modulename)

        self.module2contents[modulename] = set()

        content = self.get_content_from_module(module = module)

        # Only allowed scripts
        for content_elem in content:
            # Record which module holds which content element with both way lookup abilities
            self.module2contents[modulename].add(content_elem)
            if content_elem in self.content2module:
                self.content2module[content_elem].append(modulename)
            else:
                self.content2module[content_elem] = [modulename]
            if len(self.content2module[content_elem]) > 1:
                self.logger.warning('Content elem of type {}: {} was defined multiple times in: {}'.format(self._type, content_elem, self.content2module[content_elem]))
            setattr( self, content_elem, getattr( module, content_elem ) )

    def update(self):
        self.preupdate()
        new_hash = hash_dir_contents(self.dir) if os.path.exists(self.dir) else dict()
        if new_hash != self.hash:
            removed = set(self.hash).difference(new_hash)

            for filename in removed:
                self.remove_module(filename)

            # Adding and reloading uses the same operation so we can skip checking which files are new and which are changed
            # hash_dir_contents hashes only .py files, so it isn't necessary to check it again
            for filename in new_hash:
                if filename not in self.hash or self.hash[filename] != new_hash[filename]:
                    file_path = os.path.join(self.dir_name, filename).strip('./ ')
                    self.add_module(file_path)
        self.hash = new_hash
        self.postupdate()
