import hyperstore

import random

HS = hyperstore.HyperStore( 'what' )
HS.article_table_name = 'pyplyn-prod-all'
ass = [ {
    'annotations': article.annotations, 
    'body': article.body, 
    'title': article.title, 
    'url': article.url, 
    'source': 'max-test-annotations'
} for article in HS.scan( count = 10 ) ]
ret_ = True

def fetch( *args ):
    global ret_, ass
    if ret_:
        ret_ = False
        return [ (article, 'ProcessAnnotationsValve') for article in ass ]
    else:
        return []
