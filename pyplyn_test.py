import pytest
from collections import namedtuple
from functools import reduce
from itertools import product

from pyplyn import PyplynConfig

from helpers import permutations_pairs

ArgsKwargs = namedtuple('ArgsKwargs', ('args', 'kwargs', 'nawt'))

def test_pyplyn_config_fails_with_incorrect_arguments():

    positional_args = {'env': 0, 'name': 1}
    arguments = {
        str: ('env', 'name', 'valves_config_filepath', 'config_filepath', 'loglevel', 'default_valve_loglevel', 'scripts_dir', 'valves_dir'),
        int: ('itemcap', 'processes', 'queue_size', 'reprocess_queue_size'),
        bool: ('_empty', )
    }
    allargs = (arg for type_ in arguments for arg in arguments[type_])
    type_samples = {
        str: 'mama',
        int: 666,
        None: None,
        float: 10.5,
        bool: False,
        tuple: (),
        "not": "not"
    }


    permutations_per_argument = [[(arg, val) for val in type_samples.values()] for arg in allargs]
    



if __name__ == '__main__':
    test_pyplyn_config_fails_with_incorrect_arguments()
