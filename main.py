print( 'Importing Dependencies...' )
from pyplyn import PyplynEngine as Engine, PyplynConfig as Config

print( 'Creating Config...' )
config = Config.from_remote_config( 'https://itemstore-test.inyourarea.co.uk/items/configs?service_name=DataJuggler-test' )
config.expose_to_valves.remove( 'itemstore_url' )
config.expose_to_valves.remove( 'cognito_api_url' )
config.expose_to_valves.remove( 'ddb_all' )
config.expose_to_valves.remove( 'ddb_articles' )

print( 'Initializing Engine...' )
engine = Engine( config )

engine.start()


from time import sleep
while True:
    try:
        sleep(1)
    except KeyboardInterrupt:
        break

engine.stop()

