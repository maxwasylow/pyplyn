# Pyplyn

Pyplyn is a platform designed for fast and parallel data processing written in Python (3.6).

## Config: PyplynConfig

### Creating PyplynConfig

PyplynConfig(env, name, loglevel='INFO', default_valve_loglevel='INFO', itemcap=25, processes=25, _empty=False, queue_size=50, reprocess_queue_size: 50, scripts_dir='./pyplyn_scripts', valves_dir='./valves', config_filepath=None, valve_config_filepath=None)

    config_filepath: if None defaults to '{config.dir}/config.json'

    valve_config_filepath: if None defaults to '{config.dir}/{config.valve_path}/config.json')

    _empty: DO NOT USE! Reserved for internal use

PyplynConfig.from_environment()

    Creates PyplynConfig object pupulated with attributes taken directly from environment variables (check Attribute reference table ENV/JSON Name for environmental variables names)

PyplynConfig.from_file(filepath)

    Creates PyplynConfig object populated with attributes taken from JSON file (check Attribute ENV/JSON Name reference table for JSON attribute names)

    filepath: path to the JSON config file
                

### Attributes

| Attribute Name | Attribute ENV/JSON Name | Attribute Purpose |
| -- | -- | -- |
| dir | N/A | |
| strict | N/A | |
| env | PYPLYN_ENV | | 
| name | PYPLYN_NAME | |
| loglevel | PYPLYN_LOGLEVEL | |
| valve_loglevels | PYPLYN_VALVE_LOGLEVELS | |
| itemcap | PYPLYN_ITEMCAP | |
| processes | PYPLYN_PROCESSES | |
| queue_size | PYPLYN_QUEUE_SIZE | |
| reprocess_queue_size | PYPLYN_REPROCESS_QUEUE_SIZE | |
| config_path | PYPLYN_CONFIG_PATH | |
| valve_path | PYPLYN_VALVE_PATH | |
| valve_config_path | PYPLYN_VALVE_CONFIG_PATH | |
| valves | N/A | |

### Datajuggler own attributes
| Attribute Name | Attribute ENV/JSON Name | Attribute Purpose |
| -- | -- | -- |
| queue_name | | |
| secondary_queue_name | | |
| deadletter_queue_name | | |
| ddb_all | | |
| ddb_articles | | |
| ddb_bands | | |


## Engine: PyplynEngine

#### Engine States

* PreStartup
* Startup
* PostStartup

