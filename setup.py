from distutils.core import setup

setup(name='Pyplyn',
      version='0.1',
      description='Framework designed to help with fast and parallel data processing',
      author='Max Wasylow',
      author_email='bamwasylow@gmail.com',
      url='https://bitbucket.org/maxwasylow/pyplyn.git',
      packages=['pyplyn', 'pyplyn.core', 'pyplyn.config'],
      dependency_links = [
          'git+https://bitbucket.org/maxwasylow/helpers.git'
      ],
      install_requires = [
          'simplejson'
      ]
)
